package com.example.notes;

import com.example.notes.models.NoteRepository;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.AssertionErrors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class NotesApplicationTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private NoteRepository noteRepository;

    @Test
    public void getIndexPage() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("forward:index.html"));
    }

    @Test
    public void getNotePage() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/notes/1").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("note"))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("id", 1))
                .andExpect(model().attribute("note", noteRepository.findOne(1)));

        mvc.perform(MockMvcRequestBuilders.get("/notes/10").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("note"))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("id", 10))
                .andExpect(model().attribute("note", noteRepository.findOne(10)));
    }

    @Test
    public void getNotePageNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/notes/1000").accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/404.html"));
    }

    @Test
    public void getNotesPage() throws Exception {
        PageRequest pageRequest = new PageRequest(0, 9);
        mvc.perform(MockMvcRequestBuilders.get("/notes").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("notes"))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("done", Matchers.nullValue()))
                .andExpect(model().attribute("sort", Matchers.nullValue()))
                .andExpect(model().attribute("curPage", 0))
                .andExpect(model().attribute("notes", noteRepository.findAll(pageRequest).getContent()));

        pageRequest = new PageRequest(1, 9);
        mvc.perform(MockMvcRequestBuilders.get("/notes")
                .param("page", "1")
                .param("done", "true")
                .accept(MediaType.TEXT_HTML)
        )
                .andExpect(status().isOk())
                .andExpect(view().name("notes"))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("done", true))
                .andExpect(model().attribute("sort", Matchers.nullValue()))
                .andExpect(model().attribute("curPage", 1))
                .andExpect(model().attribute("notes", noteRepository.findByDone(true, pageRequest).getContent()));

        pageRequest = new PageRequest(1, 9, Sort.Direction.DESC, "createDate");
        mvc.perform(MockMvcRequestBuilders.get("/notes")
                .param("page", "1")
                .param("done", "true")
                .param("sort", "false")
                .accept(MediaType.TEXT_HTML)
        )
                .andExpect(status().isOk())
                .andExpect(view().name("notes"))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("done", true))
                .andExpect(model().attribute("sort", false))
                .andExpect(model().attribute("curPage", 1))
                .andExpect(model().attribute("notes", noteRepository.findByDone(true, pageRequest).getContent()));
    }

    @Test
    public void getNotesPageNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/notes?page=100&sort=true&done=false").accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/404.html"));
    }

    @Test
    public void createNote() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/notes/api").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.createDate").isNumber())
                .andExpect(jsonPath("$.done").value(false))
                .andExpect(jsonPath("$.name").value(Matchers.nullValue()))
                .andExpect(jsonPath("$.content").value(Matchers.nullValue()));
    }

    @Test
    public void updateNote() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/notes/api/3")
                .header("Content-Type", "application/json")
                .content("{\"name\":\"new name\",\"content\":\"new content\"}")
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.createDate").isNumber())
                .andExpect(jsonPath("$.done").isBoolean())
                .andExpect(jsonPath("$.name").value("new name"))
                .andExpect(jsonPath("$.content").value("new content"));

        mvc.perform(MockMvcRequestBuilders.post("/notes/api/3")
                .header("Content-Type", "application/json")
                .content("{\"done\":true}")
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.createDate").isNumber())
                .andExpect(jsonPath("$.done").value(true))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.content").isString());
    }

    @Test
    public void deleteNote() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/notes/api/5"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> AssertionErrors.assertTrue("Check note", noteRepository.findOne(5) == null));
    }
}
