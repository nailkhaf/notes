package com.example.notes.controllers;

import com.example.notes.common.Paths;
import com.example.notes.common.ViewPages;
import com.example.notes.models.Note;
import com.example.notes.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping(Paths.NOTES)
public class NotesController {

    private final NoteService noteService;

    @Autowired
    public NotesController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping
    public String notes(@RequestParam(value = "done", defaultValue = "") String done,
                        @RequestParam(value = "sort", defaultValue = "") String sort,
                        @RequestParam(value = "page", defaultValue = "0") int page,
                        @RequestParam(value = "size", defaultValue = "9") int size,
                        Model model) {

        Boolean doneBool = done.isEmpty() ? null : Boolean.parseBoolean(done);
        Boolean sortBool = sort.isEmpty() ? null : Boolean.parseBoolean(sort);

        PageRequest pageRequest;
        if (sortBool != null) {
            Sort.Direction sortDirection = sortBool ? Sort.Direction.ASC : Sort.Direction.DESC;
            pageRequest = new PageRequest(page, size, sortDirection, "createDate");
        } else {
            pageRequest = new PageRequest(page, size);
        }

        Page<Note> notePage = doneBool == null ? noteService.findPage(pageRequest)
                : noteService.findPage(pageRequest, doneBool);

        if (notePage.getContent().size() == 0) return ViewPages.redirectNotFound;

        List<Integer> pages = IntStream.range(0, notePage.getTotalPages())
                .boxed().collect(Collectors.toList());

        model.addAttribute("done", doneBool)
                .addAttribute("sort", sortBool)
                .addAttribute("curPage", page)
                .addAttribute("pages", pages)
                .addAttribute("notes", notePage.getContent());

        return ViewPages.Notes;
    }

    @GetMapping(Paths.ID)
    public String note(Model model, @PathVariable("id") int id) {
        Optional<Note> note = Optional.ofNullable(noteService.find(id));
        if (!note.isPresent()) return ViewPages.redirectNotFound;
        model.addAttribute("id", id);
        model.addAttribute("note", note.get());
        return ViewPages.Note;
    }
}
