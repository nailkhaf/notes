package com.example.notes.controllers;

import com.example.notes.common.Paths;
import com.example.notes.models.Note;
import com.example.notes.services.NoteService;
import com.example.notes.services.NoteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping(Paths.NOTES + Paths.API)
public class NotesApiController {

    private final NoteService noteService;

    @Autowired
    public NotesApiController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping
    public Note getNote() {
        Note note = new Note();
        note.setCreateDate(new Date());
        note.setDone(false);
        return noteService.save(note);
    }

    @PostMapping(Paths.ID)
    public Note putNote(@PathVariable("id") int id, @RequestBody Note note) {
        Note oldNote = noteService.find(id);
        if (oldNote == null) return null;
        return noteService.update(note, oldNote);
    }

    @DeleteMapping(Paths.ID)
    public void deleteNote(@PathVariable("id") int id) {
        try {
            noteService.delete(id);
        } catch (EmptyResultDataAccessException ignored) {
        }
    }
}
