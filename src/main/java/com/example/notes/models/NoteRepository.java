package com.example.notes.models;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface NoteRepository extends JpaRepository<Note, Integer> {
    Page<Note> findByDone(boolean done, Pageable pageable);
    long countByDone(boolean done);
}
