package com.example.notes.services;

import com.example.notes.models.Note;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NoteService {
    Note find(Integer id);

    Page<Note> findPage(Pageable pageable);

    Page<Note> findPage(Pageable pageable, Boolean done);

    Note save(Note note);

    Note update(Note note, Note oldNote);

    void delete(Integer id);

}
