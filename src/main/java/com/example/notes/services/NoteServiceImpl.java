package com.example.notes.services;

import com.example.notes.models.Note;
import com.example.notes.models.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class NoteServiceImpl implements NoteService {

    private final NoteRepository noteRepository;

    @Autowired
    public NoteServiceImpl(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public Note find(Integer id) {
        return noteRepository.findOne(id);
    }

    @Override
    public Page<Note> findPage(Pageable pageable) {
        return noteRepository.findAll(pageable);
    }

    @Override
    public Page<Note> findPage(Pageable pageable, Boolean done) {
        return noteRepository.findByDone(done, pageable);
    }

    @Override
    public Note save(Note note) {
        return noteRepository.save(note);
    }

    @Override
    public Note update(Note note, Note oldNote) {
        Optional<Note> newNote = Optional.of(note);
        newNote.map(Note::getName).ifPresent(oldNote::setName);
        newNote.map(Note::getContent).ifPresent(oldNote::setContent);
        newNote.map(Note::isDone).ifPresent(oldNote::setDone);
        return noteRepository.save(oldNote);
    }

    @Override
    public void delete(Integer id) {
        noteRepository.delete(id);
    }
}
