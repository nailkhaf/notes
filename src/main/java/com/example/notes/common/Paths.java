package com.example.notes.common;

public final class Paths {
    public static final String NOTES =  "/notes";
    public static final String API =    "/api";
    public static final String ID =     "/{id}";
}
