package com.example.notes.common;

public final class ViewPages {
    public static final String Notes = "notes";
    public static final String Note = "note";
    public static final String redirectNotFound = "redirect:/404.html";
    public static final String redirectInternalError = "redirect:/500.html";
}
