DROP SCHEMA IF EXISTS test;
CREATE DATABASE test;
ALTER DATABASE test DEFAULT CHARSET utf8;
ALTER USER 'root'@'localhost' SET PASSWORD 'root';
GRANT ALL ON test.* TO 'root'@'localhost';
