DROP SCHEMA IF EXISTS test;
CREATE DATABASE test;
USE test;
ALTER DATABASE test
DEFAULT CHARSET utf8;

CREATE TABLE notes (
  PRIMARY KEY (id),
  id          INT UNSIGNED AUTO_INCREMENT,
  name        VARCHAR(150),
  content     VARCHAR(1500),
  create_date DATETIME,
  done        BOOL,
  tags        VARCHAR(150)
);

ALTER TABLE notes
DEFAULT CHAR SET utf8;
