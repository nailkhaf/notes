let buttonNewNote = document.getElementById("buttonNewNote");
let notesDone = document.getElementsByClassName("noteDone");
let notesDelete = document.getElementsByClassName("noteDelete");
let snackbar = document.getElementById('snackbar');

buttonNewNote.addEventListener('click', () => {
    buttonNewNote.setAttribute('disabled', 'true');
    fetch("notes/api/", {method: 'GET'})
        .then(res => res.json())
        .then(note => window.location.replace(`/notes/${note.id}`))
        .catch(console.log)
});

for (let note of notesDone) {
    note.addEventListener('click', e => {
        let data = {};
        data.done = !e.target.parentElement.className.includes("mdl-color-text--green-400");
        data.id = e.target.parentElement.parentElement.getElementsByClassName('note_id')[0].innerText;
        let json = JSON.stringify(data);
        fetch(`/notes/api/${data.id}`, {
            method: "POST",
            body: json,
            headers: new Headers({
                'Content-Type': 'application/json',
            })
        })
            .then(() => {
                let className = e.target.parentElement.className;
                e.target.parentElement.className = !data.done ?
                    className.replace(" mdl-color-text--green-400", "") :
                    className + " mdl-color-text--green-400";
                let message = data.done ? "Done success" : "Undone success";
                snackbar.MaterialSnackbar.showSnackbar({message: message, timeout: 800});
            })
            .catch((err) => {
                console.log(err);
                snackbar.MaterialSnackbar.showSnackbar({message: 'Occur some error :( see console'});
            });
    });
}

for (let note of notesDelete) {
    note.addEventListener('click', e => {
        id = e.target.parentElement.parentElement.getElementsByClassName('note_id')[0].innerText;
        fetch(`/notes/api/${id}`, {method: "DELETE"})
            .then(() => {
                snackbar.MaterialSnackbar.showSnackbar({message: 'Delete success, reloading...'});
                setTimeout(() => window.location.reload(true), 1000);
            })
            .catch((err) => {
                console.log(err);
                snackbar.MaterialSnackbar.showSnackbar({message: 'Occur some error :( see console'});
            });
    })
}