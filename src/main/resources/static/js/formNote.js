let formNote = document.getElementById("formNote");
let buttonSaveNote = document.getElementById('buttonSaveNote');
let buttonDeleteNote = document.getElementById('buttonDeleteNote');
let buttonBack = document.getElementById('buttonBack');
let id = document.getElementById('id').innerText;
let snackbar = document.getElementById('snackbar');

formNote.addEventListener('submit', e => e.preventDefault());

buttonSaveNote.addEventListener('click', () => {
    let formData = new FormData(formNote);
    let data = {};
    formData.forEach((value, key) => {
        data[key] = value;
    });
    data.done = data.done !== undefined;
    let json = JSON.stringify(data);
    fetch(`/notes/api/${id}`, {
        method: "POST",
        body: json,
        headers: new Headers({
            'Content-Type': 'application/json',
        })
    })
        .then(() => {
            snackbar.MaterialSnackbar.showSnackbar({message: 'Saved success', timeout: 800});
        })
        .catch(err => {
            console.log(err);
            snackbar.MaterialSnackbar.showSnackbar({message: 'Occur some error :( see console'});
        });
});

buttonDeleteNote.addEventListener('click', () => {
    fetch(`/notes/api/${id}`, {method: "DELETE"})
        .then(() => {
            snackbar.MaterialSnackbar.showSnackbar({message: 'Deleted success, return to list', timeout: 800});
            setTimeout(() => window.location.replace('/notes'), 1000);
        })
        .catch(err => {
            console.log(err);
            snackbar.MaterialSnackbar.showSnackbar({message: 'Occur some error :( see console'});
        });
});

buttonBack.addEventListener('click', () => {
    window.history.back();
});