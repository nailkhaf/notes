1. Скрипт для создания базы данных, установки кодировки и добавления пользователя. Использовать если это необходимо:  main/resources/sql/init.sql

2. Скрипт инициалиазации БД тестовыми данными: main/resources/sql/start.sql

3. Запуск приложения Spring Boot, после установки зависимостей: com.example.notes.NotesApplication

4. Запуск docker образа: $ docker run --net host -p 8080:8080 -t nailkhaf/notes

systemctl start mysqld.service