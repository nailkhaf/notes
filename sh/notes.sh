#!/usr/bin/env bash

docker run -d \
    --name notes \
    --link demo-mysql:mysql \
    -p 8080:8080 \
    -e DATABASE_HOST=demo-mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_NAME=test \
    -e DATABASE_USER=notes \
    -e DATABASE_PASSWORD=notes \
    nailkhaf/notes