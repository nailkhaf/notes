#!/usr/bin/env bash

docker run -d \
    --name demo-mysql \
    -e MYSQL_ROOT_PASSWORD=root \
    -e MYSQL_DATABASE=test \
    -e MYSQL_USER=notes \
    -e MYSQL_PASSWORD=notes \
    mysql:latest